//
//  main.swift
//
//  Created by Birger J. Nordølum on 12/07/2019.
//
//  Read args: https://www.swiftbysundell.com/posts/launch-arguments-in-swift
//

import Foundation

let wattageIntroText = #"""


     █     █░ ▄▄▄     ▄▄▄█████▓▄▄▄█████▓ ▄▄▄        ▄████ ▓█████
    ▓█░ █ ░█░▒████▄   ▓  ██▒ ▓▒▓  ██▒ ▓▒▒████▄     ██▒ ▀█▒▓█   ▀
    ▒█░ █ ░█ ▒██  ▀█▄ ▒ ▓██░ ▒░▒ ▓██░ ▒░▒██  ▀█▄  ▒██░▄▄▄░▒███
    ░█░ █ ░█ ░██▄▄▄▄██░ ▓██▓ ░ ░ ▓██▓ ░ ░██▄▄▄▄██ ░▓█  ██▓▒▓█  ▄
    ░░██▒██▓  ▓█   ▓██▒ ▒██▒ ░   ▒██▒ ░  ▓█   ▓██▒░▒▓███▀▒░▒████▒
    ░ ▓░▒ ▒   ▒▒   ▓▒█░ ▒ ░░     ▒ ░░    ▒▒   ▓▒█░ ░▒   ▒ ░░ ▒░ ░
      ▒ ░ ░    ▒   ▒▒ ░   ░        ░      ▒   ▒▒ ░  ░   ░  ░ ░  ░
      ░   ░    ░   ▒    ░        ░        ░   ▒   ░ ░   ░    ░
        ░          ░  ░                       ░  ░      ░    ░  ░

                                ~~~

  Wattage er et lite program for å holde orden på strømavlesningene.
  Programmet har støtte for å lagre dataen som JSON eller SQLite (på
  sikt).  Visning per 12. mnd., eller et valgt intervall av brukeren.
  Den viktigste funksjonen er å kunne taste inn en takst for å se
  gjennomsnittelig forbruk og kostnad.

                                ~~~

"""#

let menu = """

    N) Legg til ny avlesning
    S) Se siste (Int) måneder
    G) Se siste 12 måneder
    R) Regn ut gjennomsnitt med pris
    M) Se meny
    Q) Avslutt

"""

// https://github.com/twostraws/SwiftOnSundays/blob/master/005%20iMultiply/iMultiply/main.swift


func inputFunction() -> String? {
    print("Valg: ", terminator: "")
    return readLine()!
}

///  Creates an object which stores the amount read from a power meter,
///  as well as the date when this reading happened.  The date is representated
///  as a Date() object.
struct MeterReading: Codable {
    private let date: Date
    private let reading: Int
    init?(date: Date, reading: Int) {
        self.date = date
        self.reading = reading
    }

    /// Displays the reading with value and date in a predefined matter.
    ///
    /// - Returns: Outputs a reading and date.
    public func display() -> String {
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .full

        return dateFormatter.string(from: date)
    }
}

func createDate() -> Date? {
    let message =
        """
        Please enter the date in the format YYYY-MM-DD.

        E.g. 2019-02-22

        """
    print(message)

    // Find a better way! https://nshipster.com/datecomponents/
    let formatter = DateFormatter()
    formatter.calendar = Calendar.current
    formatter.dateFormat = "yyyy/MM/dd"

    // Create a Date component
    // guard let date = formatter.date(from: inputChoice()!) else {
    //     return nil
    // }
    return Date()
}

func createReading() -> Int? {
    let message =
        """
        Please enter the value read from meter.

        E.g. 199222

        """
    print(message)

    // Create a reading from input and the date created above.
    guard let reading = Int(readLine()!) else {
        return nil
    }
    return reading
}

var input: String
var readings: [MeterReading] = []

print(wattageIntroText)
print(menu)

while let input = inputFunction() {
    if input.uppercased() == "Q" {
        break
    }

    switch input.uppercased() {
    case "A":
        // guard let date = self.createDate() else {
        //     print("Unable to create date from input!")
        //     break
        // }
        // // Create a reading from input and the date created above.
        // guard let meterValue = self.createReading() else {
        //     print("Unable to read meter value!")
        //     break
        // }

        // guard let reading = MeterReading(date: date, reading: meterValue) else {
        //     print("Unable to create the meter reading!")
        //     break
        // }

        // readings.append(reading)
        print("\n\tSuccessfully added reading.\n\n")
    case "G":
        print("Add graph option!")
    case "P":
        for reading in readings {
            print(reading.display())
        }
    case "M":
        print(menu)
    default:
        print("Input valid choice!")
    }
}
