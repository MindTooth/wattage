# Wattage

En liten CLI-app for å genere statestikk for strømavlesning.

## Wattage 2.0

Lagre i json

Lage strukt som holder avlesning og dato

Les inn eksisterende json/data

Lagre data separat/tenk på andre måter å lagre dataen.  Backup!!! Kopier fil til filnavn.ext til filnavn.ekt.1

### Meny

* legg til nå avslesning
* Se siste (int)
* Se siste 12 mnd
* Se gjennomsnitt siste år
* Regn ut gjennsomsnitt forbruk med pris som parameter
* Se menu (hjelpemeny)
* Avslutt

### In the future

* Benytt SQLite til å lagre dataen
* Se på Apple sine databaseteknologier
