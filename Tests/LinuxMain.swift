import XCTest

import WattageTests

var tests = [XCTestCaseEntry]()
tests += WattageTests.allTests()
XCTMain(tests)
