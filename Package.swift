// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "Wattage",
    targets: [
        .target(
            name: "Wattage"
        ),
        .testTarget(
            name: "WattageTests"
        ),
    ]
)
